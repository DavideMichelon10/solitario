/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercitazione3;

import static esercitazione3.Costanti.*;
import java.util.*;


public class Mano {
    List<Carta> mano = new LinkedList<Carta>();
    
    public boolean checkVittoria(){
        for(int i=0; i < NUMEROCARTEMANO; i++){
            for(int j=0; j< NUMEROCARTEMANO; j++){
                if(i!=j && mano.get(i).equals(mano.get(j))){
                    return true;
                }
            }
        }
        return false;
    }
    public void scarta(int i){
        mano.remove(i);
    }
    public void pescaCarta(Mazzo m){
        mano.add(m.mazzo.get(0));
        m.mazzo.remove(0);
    }
    public void stampa(){
        for(int i = 0; i< mano.size(); i++){
            mano.get(i).stampa();
        }
    }
}
