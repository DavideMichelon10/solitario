/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercitazione3;

import static esercitazione3.Costanti.*;
import java.util.*;
public class MazzoBase {
    List<Carta> mazzoBase = new LinkedList<Carta>();
    MazzoBase(){
        for(int i=0; i<GRANDEZZAMAZZO/2; i++){
            mazzoBase.add(new Carta(Carta.Seme.Cuori, i+1));
        }
        for(int i=0; i<GRANDEZZAMAZZO/2; i++){
            mazzoBase.add(new Carta(Carta.Seme.Quadri, i+1));
        }
    }
   
    public void stampa(){
        for(int i = 0; i< mazzoBase.size(); i++){
           mazzoBase.get(i).stampa();
        }
    }
   
}
