/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercitazione3;

import java.util.Objects;

/**
 *
 * @author Davide
 */

public class Carta {
    
    public enum Seme{
        Cuori, Quadri
    }
    private Seme seme;
    private int valore;
    
    Carta(Seme seme, int valore){
        this.seme = seme;
        this.valore = valore;
    }
    
    public Seme getSeme() {
        return seme;
    }

    public void setSeme(Seme seme) {
        this.seme = seme;
    }

    public int getValore() {
        return valore;
    }

    public void setValore(int valore) {
        this.valore = valore;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carta other = (Carta) obj;
        if (this.valore != other.valore) {
            return false;
        }
        if (this.seme != other.seme) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.seme);
        hash = 53 * hash + this.valore;
        return hash;
    }
    
    public void stampa(){
        System.out.println("Seme: "+this.seme+ " valore: " +this.valore);
    }
}
