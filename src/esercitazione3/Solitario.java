/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esercitazione3;

import static esercitazione3.Costanti.*;
import java.util.Deque;
import java.util.Scanner;

/**
 *
 * @author Davide
 */
public class Solitario {
    public static void main(String[] args) {
        Mazzo m = new Mazzo();
        Mano mano = new Mano();
        int indice = 0;
        m.mescola();
        System.out.println("MANO: ");
        mano = m.creaMano();
        //DISPLAY MANO
        mano.stampa();
        do{
            if(mano.checkVittoria()){
                System.out.println("BRAVO, HAI VINTO");
                return;
            }else{
                System.out.println("PECCATO, NON HAI VINTO");
                System.out.print("QUALE CARTA VUOI SCARTARE?");
                Scanner sc = new Scanner(System.in);
                System.out.println(" ");
                int i = sc.nextInt();
                mano.scarta(i);
                //PESCA
                mano.pescaCarta(m);
                System.out.println("Nuova mano con carta: ");
                mano.stampa();
            }
            indice++;
        }while(indice < NUMEROTENTATIVI);
        if(indice == 4){
            System.out.print("TENTATIVI ESAURITI, HAI PERSO!");
        }
        
    }
}
